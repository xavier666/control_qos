def change(metric, threshold):

    # put in config file
    #window = vm_config.get_qos_window()
    window = 2

    # get correct interval value
    #time_interval = vm_config.
    time_interval = 2

    count = 0

    for i in range(len(metric) - 1):

        slope = (int(metric[i+1]) - int(metric[i])) / time_interval

        print i, slope

        if (slope < threshold):
                count = 0
        else:
                count +=1

        if (count == window):
                print ("QoS Drop Detected")
                return 1,i

    return 0,-1

def negative_change(metric, threshold):

    # changing it to negative for -45 degree
    threshold *= -1

    #window = vm_config.get_qos_window()
    window = 2

    # get correct interval value
    #time_interval = vm_config.
    time_interval = 2

    count = 0

    for i in range(len(metric) - 1):

        slope = (int(metric[i+1])-int(metric[i])) / time_interval

        if (slope > threshold):
                count = 0
        else:
                count +=1

        if (count == window):
                print ("QoS Drop Detected")
                return 1,i

    return 0,-1

def main():
    met = [87,56,34,98,67,55,06,79,45,84,65,93,48]

    met = [10,20,30,40,50,60,70,80]
    met = [80,70,60,50]

    print negative_change(met, 1)

if __name__ == '__main__':
    main()
