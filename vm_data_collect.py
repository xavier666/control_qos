from ovirtsdk.api import API
from ovirtsdk.xml import params

import subprocess as sp
import random
import time
import sys
import paramiko
import threading
import csv
from multiprocessing import Process
import vm_config
import logging
import vm_qos_analysis
import calendar
import os
import vm_config
import host_config

URL             = host_config.get_ovirt_interface_url()
USERNAME        = host_config.get_ovirt_interface_login()[0]
PASSWORD        = host_config.get_ovirt_interface_login()[1]

logging.basicConfig(level=logging.WARNING,format='%(asctime)s %(created)d  %(message)s',filename='vm_monitor.log',filemode='a')


def gather_data(VM, obs, run, algo_flag, vmIPaddress):		# data gathering function

	vmName 			= vm_config.vm_user[str(VM)]
	sleep_time 		= vm_config.time_sleep
	size_of_slice 		= vm_config.size_slice
	log_fetch_size 		= vm_config.apache_log_fetch_size
	log_sample_size  	= vm_config.apache_log_sample_size
	csv_dir			= host_config.get_data_csv_dir()
	data_dst_username	= host_config.get_data_dst_username()
	data_dst_ip		= host_config.get_data_dst_ip()

	# starting the remote data gathering at the individual VMs
	while True:
		try:	#executing remote file creation
			cmd = "nohup ssh "+vmName+"@"+str(vmIPaddress)+" 'python ~/create_slice.py " \
				+str(VM)+" " \
				+str(sleep_time)+" " \
				+str(size_of_slice)+" " \
				+str(log_fetch_size)+" " \
				+str(log_sample_size)+" " \
				+str(csv_dir)+" " \
				+str(data_dst_username)+" " \
				+str(data_dst_ip)+" " \
				+">/dev/null 2>&1 &'"

			print cmd
			sp.call(cmd, shell=True)
			break
		except:
			print "unable to start python script inside vm"

	slice_no = 1

	# waiting for the csv files to arrive from the VM
	while True:

		# file_path = "/home/user/gouranga/updated_test_code/old_copy/qos_archive/qos_of_"+str(VM)+"_"+str(slice_no)+".csv"
		file_path = csv_dir  + "/qos_of_"+str(VM)+"_"+str(slice_no)+".csv"

		file_flag = 0

		while file_flag == 0:

			if os.path.isfile(file_path):
				file_flag = 1

			else:
				time.sleep(5)

        #file found! doing qos check
		print str(VM)+"_"+str(slice_no)+" file found"
		logging.warning(str(VM)+"_"+str(slice_no)+" file found and going for qos analysis")

		if algo_flag == 1:

			vm_qos_analysis.check_qos(VM,run,slice_no)

		print str(VM)+"_"+str(slice_no)+" file completed qos analysis"
		logging.warning(str(VM)+"_"+str(slice_no)+"file completed qos analysis")

		slice_no += 1
