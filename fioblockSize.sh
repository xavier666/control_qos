#$1 takes block size from console (format: 1024K, 64K, etc)
#$2 takes count from console (format: 1280, 10240, etc)




for i in {1..30}
do

      #nohup fio --name=randwrite2 --ioengine=libaio --iodepth=4 --rw=randwrite --bs=$1 --size=10G --numjobs=1 >/dev/null 2>&1 &
      nohup dd if=/dev/urandom of=randwrite2.temp bs=$1 count=$2 >/dev/null 2>&1 &
      PROC_ID=$!

      while kill -0 "$PROC_ID" >/dev/null 2>&1; do
            sleep 5s
      done

      echo "disk load TERMINATED.restarting it"
      rm randwrite2*
done


#nohup fio --name=randwrite2 --ioengine=libaio --iodepth=4 --rw=randwrite --bs=$1 --size=6G --numjobs=10 >/dev/null 2>&1 &

#echo "fio started"

