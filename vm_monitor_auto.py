from ovirtsdk.api import API
from ovirtsdk.xml import params
from multiprocessing import Process
from collections import defaultdict

import os
import subprocess
import random
import time
import sys
import paramiko
import threading
import csv
import shutil
import logging
import math

import vm_config
import host_config
import vm_data_collect
import vm_bandwidth_modifier_2
import vm_qos_analysis

sys.path.insert(0, './misc_scripts/')
from misc_functions import parseCSV_vm_list_info

URL 		= host_config.get_ovirt_interface_url()
USERNAME 	= host_config.get_ovirt_interface_login()[0]
PASSWORD 	= host_config.get_ovirt_interface_login()[1]


logging.basicConfig(level=logging.WARNING,format='%(asctime)s %(created)d  %(message)s',filename='vm_monitor.log',filemode='w')

#resets the csv files to original values
def reset_csv(initial_filepath, current_filepath):
	initial_data = vm_bandwidth_modifier_2.read_from_csv(initial_filepath)
	vm_bandwidth_modifier_2.write_to_csv(initial_data,current_filepath)

#this method starts the virtual machine.input vm1,vm2,etc and (network or disk load)
def start_vm(VM_NAME,host_name):
	api = API(url=URL, username=USERNAME, password=PASSWORD , insecure=True)	#object of type API

	#Obtaining vm service
	vm=api.vms.get(VM_NAME)

	#Setting up the placement policy
	vm_placement_params = params.VmPlacementPolicy(host=params.VM(name=host_name),  affinity = "migratable")
	vm.set_placement_policy(vm_placement_params)
	vm.update()

	try:
		if api.vms.get(VM_NAME).status.state != 'up':			#checking whether vm specified is already active or not
			api.vms.get(VM_NAME).start()          ##SO should start for every vm at start of exp

			print VM_NAME+": is starting up"
			logging.warning(VM_NAME+"is starting up")

			while api.vms.get(VM_NAME).status.state != 'up':	#loop to wait until specified vm is active and running
				time.sleep(1)

			print VM_NAME+" : started"
			logging.warning(VM_NAME+" : started")

		else:
			print VM_NAME+'already up'
			logging.warning(VM_NAME+" : already up")

		initial_hostname = host_name # initialy assigned hosted_engine
		current_hostname = api.hosts.get(id=api.vms.get(VM_NAME).get_host().get_id()).get_name() # returns which hosted_engine is having the vm

		print ("initial_hostname: "+initial_hostname+" current_hostname: "+current_hostname)
		logging.warning("initial_hostname: "+initial_hostname+" current_hostname: "+current_hostname)

	except Exception as e:
		print VM_NAME +' : Failed to Start :\n%s' % str(e)
		logging.warning(VM_NAME +' : Failed to Start :\n%s' % str(e))

	api.disconnect()
	time.sleep(vm_config.halt_time_for_complete_vm_start)	#make this configurable

def stop_vm(VM_NAME):
	api = API(url=URL, username=USERNAME, password=PASSWORD , insecure=True)
	try:
		if api.vms.get(VM_NAME).status.state != 'down':
			print 'Stop VM'
			api.vms.get(VM_NAME).stop()
			logging.warning("stopping vm")
			#print 'Waiting for VM to reach Down status'
			#while api.vms.get(VM_NAME).status.state != 'down':
				#	time.sleep(1)
		else:
			print 'VM already down'
	except Exception as e:
		print 'Stop VM:\n%s' % str(e)
	api.disconnect()

#this method starts disk load on a virtual machine
def start_disk_load(option , vmLoginName , vmIPaddress , blockSize, count):

	#this command is for starting disk load in vm
	comd = "bash ./sshlogin.sh "+str(option)+" "+str(vmLoginName)+" "+str(vmIPaddress)+" "+str(blockSize)+" "+str(count)
	os.system(comd)

#this method starts network load through client pc
def start_network_load(option , vmLoginName , clientIPaddress , file_name, serverIPaddress):
	comd = "bash ./sshlogin.sh "+str(option)+" "+str(vmLoginName)+" "+str(clientIPaddress)+" "+str(file_name)+" "+str(serverIPaddress)
	os.system(comd)

def remove_prev_fio(vmLoginName , vmIPaddress):
	cmd = "ssh "+vmLoginName+"@"+vmIPaddress+" 'rm randwrite2.temp'"
	subprocess.call(cmd,shell=True)
	print "dd  file is removed if exists" ##SO not printed in log file

def remove_prev_apache_log(vmLoginName , vmIPaddress):
	cmd = "ssh "+"root"+"@"+vmIPaddress+" 'rm /var/log/apache2/access.log'"
	subprocess.call(cmd,shell=True)

	cmd = "ssh "+"root"+"@"+vmIPaddress+" 'systemctl stop apache2.service'"
	subprocess.call(cmd,shell=True)

	time.sleep(3)

	cmd = "ssh "+"root"+"@"+vmIPaddress+" 'systemctl start apache2.service'"
	subprocess.call(cmd,shell=True)

	print "access log file is removed if exists" ##SO not printed in log file

# #reads csv file containing name of vm, type of load and duration of run
# def parseCSV(filename):
# 	table = []
# 	csvfile=open(filename,'r')
# 	reader=csv.reader(csvfile)
# 	# print 'Inside parseCSV:\nreader-'
# 	# print reader
# 	for row in reader :
# 		# print row
# 		table.append(row)

# 	csvfile.close()
# 	return table

# =================================== 22/8 Sohan
# Returns a dictionary from vm name to bandwidth (float value)
def getCurrentBandwidth(filename):
	bandwidth_vm = {}
	with open(filename,'r') as fp:
		reader=csv.reader(fp)
		for row in reader :
			bandwidth_vm[row[0]] = float(row[1])
	return bandwidth_vm

def readCsv(filename):
	data = []
	with open(filename, 'r') as fp:
		for line in fp:
			data.append(line.split(','))

# writes into <current_vm_bandwidth>: comma-separated vm name and bandwidth
# what is the data here?
def write_current_vm_bandwidth(data, filename):
	with open(filename, 'w') as fp:
		for line in data:
			fp.write('{},{},{}\n'.format(line[0], str(line[2]), line[-1]))
# =================================== 22/8 Sohan

def argument_copy(argv_file_name, argv_list):
	fp = open(argv_file_name, 'w')

	for i in xrange(len(argv_list)):

		fp.write(argv_list[i] + " ")

	fp.close()

# =================================== 29/8 Sohan
def get_vm_counts(onVMs_list):
	n_netVm, n_diskVm = 0, 0

	# counting no. of net / disk vms
	for vm in onVMs_list:
		if vm[1] == 'network':
			n_netVm += 1
		elif vm[1] == 'disk':
			n_diskVm += 1
		else:
			raise KeyError("VM Load type invalid - " + vm_current[1])

	return n_netVm, n_diskVm

# redistribution of b/w in the server where and when a new VM spawns
def redistribute_new_vm_bandwidth(vm_current, onVMs_list):

	n_netVm, n_diskVm = get_vm_counts(onVMs_list)

	load_type = str(vm_current[1])
	# counting the incoming vm
	if load_type == 'network':
		n_netVm += 1
	elif load_type == 'disk':
		n_diskVm += 1
	else:
		raise KeyError("VM Load type invalid - " + vm_current[1])

	# onVMs_list.append([vm_current[0], vm_current[1]])


	# read current bandwidth for each vm
	bandwidth_vm = getCurrentBandwidth("current_vm_bandwidth.csv")
	sumDenominator = sum(bandwidth_vm.values())

	print("sumDenominator =", str(sumDenominator))

	# Redistribute bandwidth
	if n_diskVm == 0:

		# Bandwidth to be allocated to the new VM
		newVMBandwidth = host_config.full_bandwidth_all_networks/(n_netVm * 1.0)

		if sumDenominator != 0:	# onVMs_list must be empty
			for l in onVMs_list:
				l[2] -= l[2]*newVMBandwidth/(sumDenominator*1.0)

		# b_n = host_config.full_bandwidth_all_networks/(n_netVm*1.0) # bandwidth for network vm
		diskVMBandwidth = host_config.gluster_synchronization_bandwidth # bandwidth for disk vm

	else:	# n_diskVm >= 1

		# Bandwidth to be allocated to the new VM
		newVMBandwidth = host_config.full_bandwidth_with_disks / ((n_netVm + n_diskVm)*1.0)

		# new / incoming vm is disk type
		if load_type == 'disk':
			sumNetworkBandwidth = 0
			for l in onVMs_list:
				if l[1] == 'network':
					sumNetworkBandwidth += l[2]
					l[2] -= l[2]*newVMBandwidth/(sumDenominator*1.0)
				else:
					b_d = l[2]

			if n_diskVm == 1:
				diskVMBandwidth = newVMBandwidth
			else:
				totalBandwidth = sumNetworkBandwidth + (n_diskVm-1) * b_d
				diskVMBandwidth = ((n_diskVm-1)*b_d + (newVMBandwidth*sumNetworkBandwidth)/(totalBandwidth*1.0))/(n_diskVm*1.0)


			for l in onVMs_list:
				if l[1] == 'disk':
					l[2] = diskVMBandwidth

			newVMBandwidth = diskVMBandwidth

		# new / incoming vm is network type
		else:
			for l in onVMs_list:
				l[2] -= l[2]*newVMBandwidth/(sumDenominator*1.0)

	onVMs_list.append([vm_current[0], vm_current[1], newVMBandwidth, vm_current[-1]])

	write_current_vm_bandwidth(onVMs_list, 'current_vm_bandwidth.csv')

	# APPLY BANDWIDTHS: all b/w's for networks, and diskVMBandwidth for disk

# =================================== 29/8 Sohan

# ===== 29/8 Sumitro

# element of onVMs_list: [vm_name, load_type, bandwidth, servername]
def change_all_vm_bandwidth(onVMs_list):

	# a flag to check if there are any disk VMs in the current server
	disk_flag = 0

	disk_bandwidth = 0

	for row in onVMs_list:
		if row[1] == 'network':
			vm_bandwidth_modifier_2.change_vm_bandwidth(row[0],row[2])

		else:
			# adding all disk bw in the server
			# then applying one single rule
			disk_flag = 1
			disk_bandwidth += row[2]
			server_forGlusterBw = row[-1]

	# if there exists a single disk VM in the server
	# then a gluster rule needs to be applied
	if disk_flag == 1:
		vm_bandwidth_modifier_2.change_gluster_bandwidth(server_forGlusterBw, disk_bandwidth)

def test_print_vm_bw(onVMs_list):
	for row in onVMs_list:
		print row

# ===== 29/8 Sumitro

# ===== 07/09 Sumitro

# resetting all files before every new experiment run

def reset_all_csv_files():
	try:
		reset_csv("initial_host_gluster_bandwidth.csv", "host_gluster_bandwidth.csv")	#updates host_gluster_bandwidth csv value
		print "host_gluster_bandwidth.csv updated correctly"
	except:
		print "error while updating gluster csv values"

	try:
		# Bandwidth zero means VM is OFF
		reset_csv("initial_vm_bandwidth.csv", "current_vm_bandwidth.csv")	#updates vm_bandwidth csv value
		print "current_vm_bandwidth.csv updated correctly"
	except:
		print "error while updating vm bandwidth csv values"

	try:
		reset_csv("initial_no_of_migration.csv", "current_no_of_migration.csv")	#updates no_of_migration csv value
		print "current_no_of_migration.csv updated correctly"
	except:
		print "error while updating no_of_migration csv values"

def reset_all_gluster_bw():

	host_list = host_config.hosts

	for host in host_list:
		curr_host_ip = host_config.host_ip[host]

		# no need for username and password
		# assuming logging into root with password-less ssh login
		bw_reset_cmd = "ssh root@%s 'sudo tc qdisc del dev ovirtmgmt root'" % (curr_host_ip)
		print bw_reset_cmd

		connect_flag = 0

		while connect_flag == 0:
			try:
				subprocess.call(bw_reset_cmd, shell = True)
				time.sleep(1)

				connect_flag = 1
			except:
				print "not connected trying again"

def main():

	# getting all arguments
	block_size 		= str(sys.argv[1])	# user input for dd block size
	count 			= str(sys.argv[2])	# user input for dd count value
	fetch_file_name = str(sys.argv[3])	# user input for apache server hosted filename.extension
	algo_flag 		= int(sys.argv[4])	# user input for starting or stoping bandwidth calc and migration.
	# 1 means start and 0 means don't start.

	default_sleep_duration = vm_config.default_sleep_duration	#sleep duration for last vm in one run
	exit_fl = 0

# ==========csv format changed: also includes server name======= 29/8 Sohan
	vm_list = []
	serverForVm_dict = {}
	# contains the list of vms which needed to be started
	# converting the input csv file to list and storing it in vm_list
	# serverForVm_dict stores only the server information of the VM
	parseCSV_vm_list_info("vm_list_info.csv", vm_list, serverForVm_dict)
# ======================================================== 29/8 Sohan

	process_id 	= []
	print "main :", vm_list

	# creating a hard copy of the arguments used for the experiments
	argv_file_name = "vm_exp_arg_list.txt"

	argument_copy(argv_file_name, sys.argv)

	run = 1		#hard coded for single run. by jaydeep
	obs = vm_config.number_of_observation

	reset_all_csv_files()
	reset_all_gluster_bw()

	# starting all the VMs initially
	# and creating the process ID for generating the load
	# the loads have not yet started
	for i in range(len(vm_list)):

		vm_current = vm_list[i]

		#starting vm and passing the server where it will be started.
		start_vm(vm_current[0],vm_current[-1])

		vmLoginName = vm_config.vm_user[vm_current[0]]
		vmIPaddress = vm_config.vm_ip[vm_current[0]]

		remove_prev_fio(vmLoginName , vmIPaddress) #this removes existing fio files if any

		remove_prev_apache_log(vmLoginName , vmIPaddress) #this removes existing apache access.log files if any

		process_id.append(Process(target=vm_data_collect.gather_data, args=(vm_current[0],obs,run,algo_flag,vmIPaddress)))


# ======================================================== 29/8 Sohan

	# a disctionary which contains all VMs categorized by server
	onVMsInServer_dict = defaultdict(list)

# ======================================================== 29/8 Sohan

	net_load_count = 0

	# starting the data gathering first
	# redistributing the bw
	# starting the individual loads
	for i in range(len(vm_list)):
		process_id[i].start()

		vm_current = vm_list[i]
		print vm_current[0],"data gather has started"
		logging.warning("%s data gather has started" %(str(vm_current[0])))

		time.sleep(vm_config.halt_time_before_load_start) #halt time before starting load

		load_type = str(vm_current[1])

		# ======================================================== 29/8 Sohan
		# onVMs_list, n_netVm, n_diskVm = redistribute_new_vm_bandwidth(vm_current, onVMsInServer_dict[initialVMServer], n_netVm, n_diskVm)
		spawningVMServer = vm_current[-1] # serverForVm_dict[vm_current[0]]
		redistribute_new_vm_bandwidth(vm_current, onVMsInServer_dict[spawningVMServer])
		# ======================================================== 29/8 Sohan

		# === 29/8 Sohan
		change_all_vm_bandwidth(onVMsInServer_dict[spawningVMServer])
		# === 29/8 Sohan

		test_print_vm_bw(onVMsInServer_dict[spawningVMServer])

		# === Sumitro
		# added a dict to shorten the code a bit
		load_dic = {"disk"		:1,
					"network"	:2}
		load_option = load_dic[load_type]

		# checking load type and starting the proper load at the VM
		if load_type == "disk":
			vmLoginName = vm_config.vm_user[vm_current[0]]
			vmIPaddress = vm_config.vm_ip[vm_current[0]]

			start_disk_load(load_option , vmLoginName , vmIPaddress , block_size, count)

			print " disk load started in", vm_current[0]
			logging.warning(" disk load started in %s" %(str(vm_current[0])))

		elif load_type == "network":

			# getting the IP of the web client
			# i assume that the number of network load web client machines will be less
			# so that we can reuse the machines to generate network load
			# So we wrap around the machine IP in the following lines
			net_load_client_num = vm_config.net_load_client_num
			clientIPaddress = vm_config.net_load_client_list[net_load_count % net_load_client_num]

			# getting the server IP which is hosting the web server
			serverIPaddress = vm_config.vm_ip[str(vm_current[0])]
			clientLoginName = vm_config.net_load_client_username

			start_network_load(load_option , clientLoginName , clientIPaddress , fetch_file_name, serverIPaddress)
			
			# this indicates us to use the next web client for load generation
			net_load_count += 1

			print " network load started in", vm_current[0]
			logging.warning(" network load started in %s" %(str(vm_current[0])))

		# band_data = vm_bandwidth_modifier_2.read_from_csv('current_vm_bandwidth.csv')
		# band_data[i][2] = 1
		# vm_bandwidth_modifier_2.write_to_csv(band_data,'current_vm_bandwidth.csv')
		print vm_current[0],"load has started"

		if int(vm_current[2]) < 0:
			time.sleep(default_sleep_duration)

		else:
			time.sleep(int(vm_current[2]) * 60)


	print "starting loop completed"
	time.sleep(vm_config.halt_time_before_all_vm_stop)

	# stopping all VMs
	for i in range(len(vm_list)):
		print "stoping  vm"
		vm_current = vm_list[i]
		stop_vm(vm_current[0])

if __name__=='__main__':
	main()
