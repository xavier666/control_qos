#code to analysis QOS from the collected data

from ovirtsdk.api import API
from ovirtsdk.xml import params

import subprocess
import random
import time
import sys
import paramiko
import threading
import csv
import os
from multiprocessing import Process
import shutil
import vm_config
import vm_bandwidth_modifier_2
import logging
import migrate_vm
import numpy as np

import host_config

URL             = host_config.get_ovirt_interface_url()
USERNAME        = host_config.get_ovirt_interface_login()[0]
PASSWORD        = host_config.get_ovirt_interface_login()[1]


logging.basicConfig(level=logging.WARNING,format='%(asctime)s %(created)d  %(message)s',filename='vm_monitor.log',filemode='a')


# global variables to store verious parameters for "change method" when matric is disk

first_avg_flag_disk = 0		# flag for first qos slice file identification
avg_disk = 0			# storing running average of list
stdev_disk = 0			# storing running std deviation of list
avg_elements_disk = []		# list of size "window" to store values from matric list
neglected_elements_buffer_disk = []	# list to store values that are neglected
continious_drop_flag_disk = 0		# flag to identify there is a continious drop
next_element_disk = 0			# stores the next location of list

# global variables to store verious parameters for "change method" when matric is network

first_avg_flag_net = 0
avg_net = 0.0
stdev_net = 0.0
avg_elements_net = []
neglected_elements_buffer_net = []
continious_drop_flag_net = 0
next_element_net = 0


# the below method check if there is a qos drop in the vm.
def change(metric,threshold, type_flag):

	window = vm_config.window_size		# size of sliding window
	continious_drop_count = vm_config.continious_drop_counter	# no. of continious drop point considered

	if type_flag == 'disk':		# for matric of disk load (disk busy_time)

		print "busy_time inside: ",metric
		if all(val == 0 for val in metric):	# terminates the analysis when entire qos list is having zero as value
			# this happens when the load has not started or is just closed
			# thus we get lots of zeros
			print "entire list is zero"
			return 0,-1

		cal_stanDev_disk = lambda arr : abs(round(np.std(arr),3))	# lambda expression for standerd deviation calculation

		# local copy of global variable
		global first_avg_flag_disk
		global avg_disk
		global stdev_disk
		global avg_elements_disk
		global neglected_elements_buffer_disk
		global continious_drop_flag_disk
		global next_element_disk

		if first_avg_flag_disk == 0:	# true if first qos slice file's list is received

			avg_disk = np.average(metric[:window])	#calculating average of first (window) numbers of elements in matric list
			avg_elements_disk = metric[:window]	#copying the first (window) numbers of elements in matric list to other list
			stdev_disk = cal_stanDev_disk(avg_elements_disk)	#calculating the std deviation  of the new list
			first_avg_flag_disk = 1
			print avg_disk," ",stdev_disk," ",avg_elements_disk

			for counter in range(len(metric) - window):	# loop to check remaining elements in the first qos slice file's list
				print "inside loop ",avg_disk," ",stdev_disk," ",int(metric[counter+window])," ",avg_elements_disk

				# new value is within tolerable limits
				# pop oldest value
				# push new values
				if int(metric[counter+window]) < (avg_disk + stdev_disk): #next value is lesser than sum of avg and std deviation
					avg_elements_disk.pop(0)	# deleting oldest element from the list
					avg_elements_disk.append(int(metric[counter+window])) #inserting the current element in the list
					# recalculating avg and std deviation
					avg_disk = np.average(avg_elements_disk)
					stdev_disk = cal_stanDev_disk(avg_elements_disk)
					print "inside if ",avg_disk," ",stdev_disk," ",int(metric[counter+window])," ",avg_elements_disk

				# new value is exceeding avg + stdev
				elif int(metric[counter+window]) > (avg_disk + stdev_disk): #next value is gretter than sum of avg and std deviation (drop detected)

					neglected_elements_buffer_disk.append(int(metric[counter+window])) #inserting the current element in a tempoary buffer

					if (counter+window) != next_element_disk:	# checks for continious drop
						continious_drop_flag_disk = 0	# reset continious drop flag if there is no continious drop

						# inserts values from temporary buffer to global buffer
						for count in range(len(neglected_elements_buffer_disk)):
							val = neglected_elements_buffer_disk.pop(0)
							avg_elements_disk.pop(0)
							avg_elements_disk.append(int(val))
						del neglected_elements_buffer_disk[:]	# deletes content of temporary buffer

					continious_drop_flag_disk += 1	# increments the flag if there is a continious drop

					# this is the expected position in metric list of
					# the next qos drop. If this does not match (shown in above code), Then there
					# is no consecutive QoS drop, and a flag is reset

					next_element_disk = counter + window + 1  #incremets the pointer to next element for compairing continious drop

					if next_element_disk == len(metric):	# resets the pointer if it is the last element of the list
										# this is a wrap-over for the next list
						next_element_disk = 0

					print "inside elif ",avg_disk," ",stdev_disk," ",int(metric[counter+window])," ",neglected_elements_buffer_disk," ",continious_drop_flag_disk

				if continious_drop_flag_disk == continious_drop_count: # condition when continious drop is detected
					print ("QoS Drop Detected")

					continious_drop_flag_disk = 0	# resets the flage to zero for reuse

					# inserts values from temporary buffer to global buffer
					# the next avg is calculated here using the dropped Qos values
					# this is done so that the vm does not request for resource multiple times

					for count in range(len(neglected_elements_buffer_disk)):
						val = neglected_elements_buffer_disk.pop(0)
						avg_elements_disk.pop(0)
						avg_elements_disk.append(int(val))

					del neglected_elements_buffer_disk[:]	# deletes content of temporary buffer
					#recaluclates average and std deviation
					avg_disk = np.average(avg_elements_disk)
					stdev_disk = cal_stanDev_disk(avg_elements_disk)

					return 1,counter	# returns values to indicate drop, denoted by 1
								# counter indicated position of qos drop

		# this is for 2nd and all metric list after the 2nd list
		# the main change is that the window is no longer required as
		# the window already exists from the previous section of the code

		else:	# condition when second and above qos slice file's list is received
			for counter in range(len(metric)):# loop to check all elements in the qos slice file's list
				print "inside loop ",avg_disk," ",stdev_disk," ",int(metric[counter])," ",avg_elements_disk
				if int(metric[counter]) < (avg_disk + stdev_disk):	#next value is lesser than sum of avg and std deviation
					avg_elements_disk.pop(0)	# deleting oldest element from the list
					avg_elements_disk.append(int(metric[counter]))	#inserting the current element in the list
					# recalculating avg and std deviation
					avg_disk = np.average(avg_elements_disk)
					stdev_disk = cal_stanDev_disk(avg_elements_disk)
					print "inside if ",avg_disk," ",stdev_disk," ",int(metric[counter])," ",avg_elements_disk

				elif int(metric[counter]) > (avg_disk + stdev_disk): #next value is gretter than sum of avg and std deviation (drop detected)

					neglected_elements_buffer_disk.append(int(metric[counter]))	#inserting the current element in a tempoary buffer

					if (counter) != next_element_disk:	# checks for continious drop
						continious_drop_flag_disk = 0	# reset continious drop flag if there is no continious drop
						# inserts values from temporary buffer to global buffer
						for count in range(len(neglected_elements_buffer_disk)):
							val = neglected_elements_buffer_disk.pop(0)
							avg_elements_disk.pop(0)
							avg_elements_disk.append(int(val))
						del neglected_elements_buffer_disk[:]	# deletes content of temporary buffer


					continious_drop_flag_disk += 1	# increments the flag if there is a continious drop
					next_element_disk = counter + 1	# incremets the pointer to next element for compairing continious drop

					if next_element_disk == len(metric): # resets the pointer if it is the last element of the list
						next_element_disk = 0
					print "inside elif ",avg_disk," ",stdev_disk," ",int(metric[counter])," ",neglected_elements_buffer_disk," ",continious_drop_flag_disk

				if continious_drop_flag_disk == continious_drop_count: # condition when continious drop is detected
					print ("QoS Drop Detected")

					continious_drop_flag_disk = 0	# resets the flage to zero for reuse

					# inserts values from temporary buffer to global buffer
					for count in range(len(neglected_elements_buffer_disk)):
						val = neglected_elements_buffer_disk.pop(0)
						avg_elements_disk.pop(0)
						avg_elements_disk.append(int(val))

					del neglected_elements_buffer_disk[:]	# deletes content of temporary buffer
					#recaluclates average and std deviation
					avg_disk = np.average(avg_elements_disk)
					stdev_disk = cal_stanDev_disk(avg_elements_disk)

					return 1,counter	# returns values to indicate drop


		return 0,-1	# returns values to indicate  there is no drop

	if type_flag == 'net':	# for matric of network load (apache process_time)

		print "process_time inside: ",metric
		if all(val == 0.0 for val in metric):
			print "entire list is zero"
			return 0,-1

		# lambda expression for standerd deviation calculation.
		# increasing the value 4 fold since value is very very small
		cal_stanDev_net = lambda arr : abs(round(np.std(arr),8))*4

		global first_avg_flag_net
		global avg_net
		global stdev_net
		global avg_elements_net
		global neglected_elements_buffer_net
		global continious_drop_flag_net
		global next_element_net

		if first_avg_flag_net == 0:

			avg_net = np.average(metric[:window])
			avg_elements_net = metric[:window]
			stdev_net = cal_stanDev_net(avg_elements_net)
			first_avg_flag_net = 1
			print avg_net," ",stdev_net," ",avg_elements_net

			for counter in range(len(metric) - window):
				print "inside loop ",avg_net," ",stdev_net," ",float(metric[counter+window])," ",avg_elements_net
				if float(metric[counter+window]) < (avg_net + stdev_net):
					avg_elements_net.pop(0)
					avg_elements_net.append(float(metric[counter+window]))
					avg_net = np.average(avg_elements_net)
					stdev_net = cal_stanDev_net(avg_elements_net)
					print "inside if ",avg_net," ",stdev_net," ",float(metric[counter])," ",avg_elements_net

				elif float(metric[counter+window]) > (avg_net + stdev_net):

					neglected_elements_buffer_net.append(float(metric[counter+window]))

					if (counter+window) != next_element_net:
						continious_drop_flag_net = 0
						for count in range(len(neglected_elements_buffer_net)):
							val = neglected_elements_buffer_net.pop(0)
							avg_elements_net.pop(0)
							avg_elements_net.append(float(val))
						del neglected_elements_buffer_net[:]

					continious_drop_flag_net += 1
					next_element_net = counter + window + 1

					if next_element_net == len(metric):
						next_element_net = 0
					print "inside elif ",avg_net," ",stdev_net," ",float(metric[counter+window])," ",neglected_elements_buffer_net," ",continious_drop_flag_net


				if continious_drop_flag_net == continious_drop_count:
					print ("QoS Drop Detected")

					continious_drop_flag_net = 0
					for count in range(len(neglected_elements_buffer_net)):
						val = neglected_elements_buffer_net.pop(0)
						avg_elements_net.pop(0)
						avg_elements_net.append(float(val))

					del neglected_elements_buffer_net[:]
					avg_net = np.average(avg_elements_net)
					stdev_net = cal_stanDev_net(avg_elements_net)

					return 1,counter

		else:
			for counter in range(len(metric)):
				print "inside loop ",avg_net," ",stdev_net," ",float(metric[counter])," ",avg_elements_net
				if float(metric[counter]) < (avg_net + stdev_net):
					avg_elements_net.pop(0)
					avg_elements_net.append(float(metric[counter]))
					avg_net = np.average(avg_elements_net)
					stdev_net = cal_stanDev_net(avg_elements_net)
					print "inside if ",avg_net," ",stdev_net," ",float(metric[counter])," ",avg_elements_net

				elif float(metric[counter]) > (avg_net + stdev_net):

					neglected_elements_buffer_net.append(float(metric[counter]))

					if (counter) != next_element_net:
						continious_drop_flag_net = 0
						for count in range(len(neglected_elements_buffer_net)):
							val = neglected_elements_buffer_net.pop(0)
							avg_elements_net.pop(0)
							avg_elements_net.append(float(val))
						del neglected_elements_buffer_net[:]


					continious_drop_flag_net += 1
					next_element_net = counter + 1

					if next_element_net == len(metric):
						next_element_net = 0
					print "inside elif ",avg_net," ",stdev_net," ",float(metric[counter])," ",neglected_elements_buffer_net," ",continious_drop_flag_net

				if continious_drop_flag_net == continious_drop_count:
					print ("QoS Drop Detected")

					continious_drop_flag_net = 0
					for count in range(len(neglected_elements_buffer_net)):
						val = neglected_elements_buffer_net.pop(0)
						avg_elements_net.pop(0)
						avg_elements_net.append(float(val))

					del neglected_elements_buffer_net[:]
					avg_net = np.average(avg_elements_net)
					stdev_net = cal_stanDev_net(avg_elements_net)

					return 1,counter

		return 0,-1

# metric - 3 -> disk, 4 -> network
# returns whether a metric has shown drop in QoS
def analysis_qos(VM , run,slice_no ):				# read data from csv file and analysis them
	file_path = "qos_archive/"+"qos_of_"+VM+"_"+str(slice_no)+".csv"

	tim= ''
	try :
		csvfile=open(file_path,'r')
		reader=csv.reader(csvfile)
		data_time  = []
		packet_drop_in = []
		packet_drop_out = []
		busy_time = []
		bytes_sent = []
		bytes_recv = []
		process_time = []
		for row in reader :
			#print row
			data_time.append(row[0])
			packet_drop_in.append(int(row[1]))
			packet_drop_out.append(int(row[2]))
			busy_time.append(int(row[3]))
			bytes_sent.append(int(row[4]))
			bytes_recv.append(int(row[5]))
			process_time.append(float(row[6]))

		csvfile.close()
		print "busy_time: ",busy_time
		print "process_time: ",process_time

		# once qos analysis is done, append checked to the file name
		new_file_path = "qos_archive/"+"qos_of_"+VM+"_"+str(slice_no)+"_Checked"+".csv"
		shutil.copy2(file_path , new_file_path)

		# delete original file
		subprocess.call(['rm', file_path])
		#cmd = "cp "+file_path+" "+new_file_path
		#subprocess.call("mv "+file_path+" "+new_file_path , shell = True)
		#os.remove(file_path)
		#shutil.copy2(file_path , new_file_path)
		#os.remove(file_path)
		#subprocess.check_output(['rm', file_path])
		threshold = 1

		# for disk load
		busy_time_change,index=change(busy_time,threshold,'disk')

		if busy_time_change==1:
			print "busy time  is more than threshold"
			logging.warning ("busy time is more than threshold at %s  ", VM)
			tim=data_time[index]

			return 3,tim

		# for net load
		process_time_change,index=change(process_time,threshold,'net')

		if process_time_change==1:
			print "process time is more than threshold"
			logging.warning ("process time is more than threshold at %s  ", VM)
			tim=data_time[index]

			return 4,tim

	except IOError :
		print " Error opening file "
		logging.warning("at qos_analysis_check : Error opening file")
		time.sleep(2)
	#subprocess.call(['rm', file_path])
	return 0, tim

def check_qos(VM , run,slice_no ):
	print "vm: "+VM+" slice: "+str(slice_no)+" is under QOS check"
	logging.warning("vm: "+VM+" slice: "+str(slice_no)+" is under QOS check")

	#print "VM"+VM+"run :"+str(run)+"slice"+str(slice_no)
	#logging.warning("VM"+VM+"run :"+str(run)+"slice"+str(slice_no))

	metric , timee = analysis_qos(VM , run,slice_no)    #find if QoS drop is detected or not

	if metric != 0 :
		print "QoS drop in "+VM+" at time :"+str(timee)
		logging.warning("QoS drop in "+VM+" at time :"+str(timee))

		host = migrate_vm.get_hostname(VM)
		print VM+"is in"+host
		logging.warning(VM+"is in"+host)

		vm_on_host = vm_bandwidth_modifier_2.get_vm_on_host(host)
		#print vm_on_host
		print "VMs in host"+host+" : "+str(vm_on_host)
		logging.warning("VMs in host"+host+" : "+str(vm_on_host))

		k = vm_config.bandwidth_modification_percentage

		if metric == 3 :
			disk_flag = 1
		else :
			disk_flag = 0
		vm_bandwidth_modifier_2.calculate_bandwidth(VM,host, vm_on_host , k , disk_flag)    # call the bandwidth modifier module
