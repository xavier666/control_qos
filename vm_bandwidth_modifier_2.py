from ovirtsdk.api import API
from ovirtsdk.xml import params


import subprocess
import os
import random
import time
import sys
import paramiko
import threading
import csv
from multiprocessing import Process
import vm_config
import host_config
import vm_data_collect
import vm_qos_analysis
import random
import logging
import migrate_vm

sys.path.insert(0, './misc_scripts/')
from misc_functions import parseCSV_vm_list_info as parse_test_case_csv

URL             = host_config.get_ovirt_interface_url()
USERNAME        = host_config.get_ovirt_interface_login()[0]
PASSWORD        = host_config.get_ovirt_interface_login()[1]


logging.basicConfig(level=logging.WARNING,format='%(asctime)s %(created)d  %(message)s',filename='vm_monitor.log',filemode='a')

# gets the server name where the VM resides
def get_hostname(VM) :
	api = API(url=URL, username=USERNAME, password=PASSWORD , insecure=True)
	hostname = api.hosts.get(id=api.vms.get(VM).get_host().get_id()).get_name()
	api.disconnect()
	return hostname

# gets the list of VMs at the mentioned server (host)
def get_vm_on_host(host) :
	api = API(url=URL, username=USERNAME, password=PASSWORD , insecure=True)
	# get all vms (up or down)
	vms = vm_config.get_vms()
	#print host
	vm_on_host = []
	for VM in vms:
		try:
			# if the vm is up
			if api.vms.get(VM).status.state == 'up':
				hostname = api.hosts.get(id=api.vms.get(VM).get_host().get_id()).get_name()

				# check if that vm exists in the host
				if hostname == host :
					vm_on_host.append(VM)

		except :
			continue
	api.disconnect()
	return vm_on_host

# wait till the VM has been migrated and up again
def wait(vm_name):
	api = API(url=URL, username=USERNAME, password=PASSWORD , insecure=True)
	#Obtaining vm service
	vm=api.vms.get(vm_name)
	state=vm.status.state
	api.disconnect()

	while state != 'up':
		#Establishing Connection
		api2=API(url=URL, username=USERNAME, password=PASSWORD , insecure=True)
		#Obtaining vm service
		vm=api2.vms.get(vm_name)

		state=vm.status.state

		api2.disconnect()
		time.sleep(2)

	print "VM "+vm_name+' has migrated'
	loggin.warning("VM "+vm_name+' has migrated')

#function to read from CSV
def read_from_csv(filepath) :
	csvfile=open(filepath,'r')
	reader=csv.reader(csvfile)
	data = []
	for row in reader :
		data.append(row)
	csvfile.close()
	return data

#function to write to csv
def write_to_csv (data , filepath):
	csvfile=open(filepath, "w")
	writer =  csv.writer(csvfile)
	for row in data :
		writer.writerow(row)
	csvfile.close()

# login to the VM and changes the bandwidth ...bandwidths are in Megabits per sec
def change_vm_bandwidth(VM,bandwidth):

	vm_ip		= str(vm_config.get_vmip(VM))
	vm_uname	= str(vm_config.get_user(VM))
	vm_pw		= vm_config.get_password(VM)
	bw 			= str(bandwidth)

	cmd="ssh root@%s 'cd /home/%s && sh ./bandwidth_choking_rules.sh %s'" %(vm_ip,vm_uname,bw)
	print cmd
	connect_flag = 0
	while connect_flag == 0:
		try:
			subprocess.call(cmd,shell=True)
			time.sleep(5)
			connect_flag = 1
		except:
			print "not connected trying again"

	#cmmd="echo "+pw+" | sudo -S ./bandwidth_choking_rules.sh "+str(bandwidth)

# login to the server and changes the gluster port bandwidth ...bandwidths are in Megabits per sec
def change_gluster_bandwidth(host,bandwidth):

	client 			= paramiko.SSHClient()
	hostname		= host_config.get_hostip(host)
	uname			= host_config.get_user(host)
	pw				= host_config.get_password(host)
	port_1, port_2 	= host_config.get_gluster_port(host)

	client.load_system_host_keys()
	client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

	connect_flag = 0

	cmmd="echo %s | sudo -S ./misc_scripts/disk_bandwidth_restriction_rule.sh %s %s %s" % (pw, str(bandwidth), port_1, port_2)
	print cmmd

	while connect_flag == 0:
		try:
			client.connect(hostname, username = uname, password = pw)
			client.exec_command(cmmd)
			client.close()
			time.sleep(5)

			connect_flag = 1
		except:
			print "not connected trying again"

def redist_bw_vm(vm_id):
	'''
	find in which host the vm is currently at -> H
	get bw of all vms at H
	redistribute the bw in the inverse ratio
	'''

	# the server in which the migrating vm originally resided
	host_server_id = get_hostname(vm_id)

	# the vms in the above server
	host_vm_list = get_vm_on_host(host_server_id)

	# bw of all the vms in all servers
	current_bandwidth_data = read_from_csv("current_vm_bandwidth.csv")

	# will store bw of the vms only in hosted_server_id
	host_vm_list_bw = []

	# will save the bw of the VM which is migrating
	bw_redist = 0

	for row in current_vm_bandwidth:

		# getting bw which needs to be redistributed
		if row[0] == vm_id:
			bw_redist = row[1]

		# extracting the bw of the vms which are in host_server_id
		elif row[0] in host_vm_list:
			host_vm_list_bw.append(row)

	# getting only the bw portion of the list
	host_vm_only_bw_list = [row[1] for row in host_vm_list_bw]

	# getting the bw partition of the remaining bw
	redist_bw_list = redist_bw_algo(bw_redist, host_vm_only_bw_list)

	# adding the excess to each vm
	for i in xrange(len(host_vm_list_bw)):
		host_vm_list_bw[i][1] += redist_bw_list[i]

	# =================================== 29/8 Sohan
	# getting input test file (the one given by user)
	input_test_case_list = []
	_ = {}
	parse_test_case_csv("vm_list_info.csv", input_test_case_list, _)
	# =================================== 29/8 Sohan

	# changing bw of VMs according to their load type
	for i in xrange(len(host_vm_list_bw)):

		for j in xrange(len(input_test_case_list)):

			if host_vm_list_bw[i] == input_test_case_list[j]:

				print host_vm_list_bw[i]

				if input_test_case_list[1] == 'network':
					# change bw from inside the vm
					change_vm_bandwidth(host_vm_list_bw[i][0], host_vm_list_bw[i][1])

				elif input_test_case_list[1] == 'disk':
					# change bw of the gluster
					# this code is currently wrong
					# gluster bw needs to be separately handled
					change_vm_bandwidth(host_vm_list_bw[i][0], host_vm_list_bw[i][1])

def redist_bw_algo(bw_redist, bw_list):
	'''
	basic logic
	4 2 1 are bw of 3 vms
	3 needs to be redistributed to the 3 vms
	so invert the bw
	distribute them by proportion
	'''
	# will contain the bw but divided by 1
	bw_list_inv = [1 / (i * 1.0) for i in bw_list]

	# the proportionality constant
	prop_const = bw_redist / (sum(bw_list_inv) * 1.0)

	# finding the bw redistribution order
	# this sum should be bw_redist
	redist_bw_list = [i * prop_const for i in bw_list_inv]

	return redist_bw_list

def calculate_bandwidth(VM,host, vm_on_host, k, disk_flag) :
	p = vm_config.maximum_donation_percentage
	if disk_flag == 0 : 					#QOS is dropped in Network parameter
		current_bandwidth_data = read_from_csv("current_vm_bandwidth.csv")
		initial_bandwidth_data = read_from_csv("initial_vm_bandwidth.csv")
		new_bandwidth = 0	# this was initially an array, corrected to a simple variable
		vm_bandwidth = 0	#global defination for vm_bandwidth
		vm_index = 0		#global definition for vm_index
		bandwidth_gain = 0
		new_bandwidth_data = []
		for row in current_bandwidth_data:

			# for each VM running on the host same as QoS affected VM
			if row[0] in vm_on_host:

				# if the vm same as the VM that is affected
				if row[0] == VM :
					vm_bandwidth = int(row[1])     # bandwidth of  the QoS affected VM
					vm_index = current_bandwidth_data.index(row)
					print "val - row (for loop): ", row
					print "val - vm_index (for loop) : ", vm_index

				else :
					index = current_bandwidth_data.index(row)
					prev_bandwidth 	= int(row[1])     #bandwidth in megabit
					init_bandwidth 	= int(initial_bandwidth_data[index][1])
					change_in_bandwidth = ((prev_bandwidth*k)/100)
					#change_in_bandwidth = (prev_bandwidth*k)/100    # k % of the current bandwidth of other VM
					new_bandwidth = prev_bandwidth - change_in_bandwidth   # new bandwidth after decreasing k%
					minimum_bandwidth = init_bandwidth * (100 - p) /100   # the minimum bandwidth threshold

					if new_bandwidth > minimum_bandwidth :     # check if new bandwidth will be higher than minimum bandwidth threshold

						change_vm_bandwidth(row[0] , new_bandwidth)    #if yes, then change bandwidth to new bandwidth
						print "bandwidth changed in VM "+row[0]+" from "+str(prev_bandwidth)+" to "+str(new_bandwidth)
						logging.warning("bandwidth changed in %s from %s to %s ...", row[0],str(prev_bandwidth),str(new_bandwidth))
						bandwidth_gain += change_in_bandwidth   # k% bandwidth is gain to QoS affected Vm

					else :
						new_bandwidth = prev_bandwidth

			else :
				new_bandwidth = int(row[1])
			new_row=[]
			new_row.append(row[0])
			new_row.append(new_bandwidth)
			new_bandwidth_data.append(new_row)

		# bandwidth gain not possible so migration needed
		if(bandwidth_gain == 0) :

			print "new_bandwidth_data:",new_bandwidth_data
			print "vm_bandwidth "+str(vm_bandwidth)
			print "val - vm_index : ", vm_index

			new_bandwidth_data[vm_index][1] = vm_bandwidth

			print "Migration Needed"

			logging.warning("migration needed for %s ..." , VM)

			#migrateVM method is called from migrate_vm.py file not from bandwidth_modifier_2.py file since api was creating problem. by jaydeep
			migrate_vm.migrateVM(VM,host)

			# after migration, redistribute the bw
			redist_bw_vm(VM)

		# add total gain in bandwidth to QoS affected VM
		else :
			modified_bandwidth = vm_bandwidth + bandwidth_gain
			new_bandwidth_data[vm_index][1] = modified_bandwidth
			change_vm_bandwidth(VM , modified_bandwidth)
			print "bandwidth changed in VM "+VM+" from "+str(vm_bandwidth)+" to "+str(modified_bandwidth)
			logging.warning("bandwidth changed in %s from %s to %s ...", VM,str(vm_bandwidth),str(modified_bandwidth))

		# write to file the change in bw
		write_to_csv (new_bandwidth_data , "current_vm_bandwidth.csv")  # write all new bandwidths

		# increase in gluster bandwidth
		if bandwidth_gain !=0 :
			gluster_bandwidth_data = read_from_csv ("host_gluster_bandwidth.csv")
			new_bandwidth = []
			for row in gluster_bandwidth_data :
				bandwidth = int(row[1])
				if row[0] == host :
					bandwidth = bandwidth - change_in_bandwidth
					change_gluster_bandwidth(host,bandwidth)
				new_row = []
				new_row.append(row[0])
				new_row.append(bandwidth)
				new_bandwidth.append(new_row)
			write_to_csv(new_bandwidth, "host_gluster_bandwidth.csv")

	if disk_flag == 1 :
		current_bandwidth_data = read_from_csv("current_vm_bandwidth.csv")
		initial_bandwidth_data = read_from_csv("initial_vm_bandwidth.csv")
		new_bandwidth = 0	# this was initially an array, corrected to a simple variable
		vm_bandwidth = 0	#global defination for vm_bandwidth
		vm_index = 0		#global definition for vm_index
		bandwidth_gain = 0
		new_bandwidth_data = []
		for row in current_bandwidth_data :
			print row[0]
			if row[0] in vm_on_host :
				print row[0]
				if row[0] == VM :
					vm_bandwidth = int(row[1])
					vm_index = current_bandwidth_data.index(row)
				else :
					index = current_bandwidth_data.index(row)
					prev_bandwidth = int(row[1])     #bandwidth in megabit
					init_bandwidth = int(initial_bandwidth_data[index][1])
					change_in_bandwidth = (prev_bandwidth*k)/100
					new_bandwidth = prev_bandwidth - change_in_bandwidth
					minimum_bandwidth = init_bandwidth * (100 - p) /100
					if new_bandwidth > minimum_bandwidth :
						change_vm_bandwidth(row[0] , new_bandwidth)
						print "bandwidth changed in VM "+row[0]+" from "+str(prev_bandwidth)+" to "+str(new_bandwidth)
						logging.warning("bandwidth changed in %s from %s to %s ...", row[0],str(prev_bandwidth),str(new_bandwidth))
						bandwidth_gain += change_in_bandwidth
						#bandwidth_gain += change_in_bandwidth
					else :
						new_bandwidth = prev_bandwidth

			else :
				new_bandwidth = int(row[1])
			new_row=[]
			new_row.append(row[0])
			new_row.append(new_bandwidth)
			new_bandwidth_data.append(new_row)
		#print vm_bandwidth
		if(bandwidth_gain == 0) :   # bandwidth gain not possible so migration needed
			new_bandwidth_data[vm_index][1] = vm_bandwidth
			print "Migration Needed"
			logging.warning("migration needed for %s ..." , VM)
			migrate_vm.migrateVM(VM,host)	# migrateVM method is called from migrate_vm.py file
							# not from bandwidth_modifier_2.py file since api was creating problem. by jaydeep
		else :
			modified_bandwidth = vm_bandwidth + bandwidth_gain
			new_bandwidth_data[vm_index][1] = modified_bandwidth
			change_vm_bandwidth(VM , modified_bandwidth)
			print "bandwidth changed in VM "+VM+" from "+str(vm_bandwidth)+" to "+str(modified_bandwidth)
			logging.warning("bandwidth changed in %s from %s to %s ...", VM,str(vm_bandwidth),str(modified_bandwidth))

		write_to_csv (new_bandwidth_data , "current_vm_bandwidth.csv")

		if bandwidth_gain !=0 :

			print "gluster_bandwidth changed"
			logging.warning("gluster bandwidth changed")
			gluster_bandwidth_data = read_from_csv ("host_gluster_bandwidth.csv")
			new_bandwidth = []
			for row in gluster_bandwidth_data :
				bandwidth = int(row[1])
				if row[0] == host :
					bandwidth = bandwidth + change_in_bandwidth
					change_gluster_bandwidth(host,bandwidth)
				new_row = []
				new_row.append(row[0])
				new_row.append(bandwidth)
				new_bandwidth.append(new_row)
			write_to_csv(new_bandwidth , "host_gluster_bandwidth.csv")
