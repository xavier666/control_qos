import csv

# =================================== 29/8 Sohan
# reads the input file vm_list_info.csv file. Format: vm_name<comma>load_type<comma>sleep_time<comma>server_name
def parseCSV_vm_list_info(filename, vm_list, serverForVm_dict):
	# data = []
	with open(filename,'r') as fp:
		reader=csv.reader(fp)
		for row in reader :
			# Assumming host names for the vms are in the last column *****
			vm_list.append(row)
			serverForVm_dict[row[0]] = row[-1]

	# return data
# =================================== 29/8 Sohan
