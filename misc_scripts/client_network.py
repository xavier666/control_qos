import psutil
import calendar
import time
import csv


def collect_QOS():
	QOSList = []
	QOSList.append(int(psutil.net_io_counters().bytes_sent))
	QOSList.append(int(psutil.net_io_counters().bytes_recv))

	return QOSList

def main():
	
	old_qos = collect_QOS()
	sliced_result = []	#for sliced data collect info. by jaydeep
	i = 1	#counter for observation no. count.by jaydeep
	slice_size = 10
	sleep_time = 1
	file_name = "client_network_status"+".csv"	#write to sliced csv file.
	while True:				#collect the data infinitely
		time.sleep(sleep_time)
		stat = []
		new_qos = collect_QOS()
		#stat.append(time.ctime(int(time.time())))
		stat.append(calendar.timegm(time.gmtime()))		
		for j in range(len(new_qos)):
			a=int(new_qos[j])-int(old_qos[j])
			stat.append(a)

		sliced_result.append(stat)	#appending qos data for sliced data collect. by jaydeep	
		
		if i % slice_size == 0:
			csvfile=open(file_name,'a')
			writer=csv.writer(csvfile)

			for row in sliced_result:
				writer.writerow(row)

			csvfile.close()	
			sliced_result = []	#reinitializing the sliced result list to empty(0).by jaydeep

		old_qos=new_qos
		i+=1


if __name__=='__main__':
	main() 

