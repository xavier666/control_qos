#!/bin/bash

#dest_dir="../qos_archive/"

mv ../vm_monitor.log ../qos_archive/
cp ../vm_list_info.csv ../qos_archive/
mv ../vm_exp_arg_list.txt ../qos_archive/
cp ../current_no_of_migration.csv ../qos_archive/

# removing trash files
rm ../*.pyc

# killing spare ssh sessions to the individual VMs
pgrep ssh -af | grep 'ssh vm' | cut -d' ' -f1 | xargs sudo kill -9 $1
