import subprocess as sp
import sys

def main():
	vm_name_and_pass = {
                #"vm1":"vm1", 
                #"vm2":"vm4",
		#"vm5":"vm1",
                #"vm6":"vm4"}
                "vm5":"vm1"}

	vm_list = {
                #"vm1":"10.5.20.11", 
                #"vm2":"10.5.20.12",
		#"vm5":"10.5.20.15", 
                #"vm6":"10.5.20.16"}
		"vm5":"10.5.20.15"}

	# file to be sent to each VM
	INSIDE_VM_SCRIPT = sys.argv[1]

	# command to be executed at 10.5.20.121
	cmd_str = "scp ./%s %s@%s:~/"

	for key in vm_list:

		exec_str = cmd_str % (INSIDE_VM_SCRIPT, vm_name_and_pass[key], vm_list[key])
		print exec_str

		sp.call(exec_str, shell = True)

if __name__ == "__main__":
    main()
