import psutil
import time
import subprocess
import os
import sys
import calendar
import csv
import logging


logging.basicConfig(level=logging.WARNING,format='%(asctime)s %(created)f  %(message)s',filename='slice.log',filemode='w')

def collect_QOS():		#function to collect QOS data from VM
	QOSList=[]
	
	QOSList.append(int(psutil.net_io_counters().dropin))
	QOSList.append(int(psutil.net_io_counters().dropout))
	QOSList.append(int(psutil.disk_io_counters().busy_time))
	QOSList.append(int(psutil.net_io_counters().bytes_sent))
	QOSList.append(int(psutil.net_io_counters().bytes_recv))
	
	return QOSList

# arg list
# > file to be sent
# > dir of destination
# > destination username
# > destination IP
def send_gen_file(file_name, csv_dir, data_dst_username, data_dst_ip):

	#csv_dir = "/home/user/gouranga/updated_test_code/old_copy/qos_archive/"
	#cmd = "scp ./"+file_name+" user@10.5.20.121:"+csv_dir

	cmd = "scp ./%s %s@%s:%s" % (file_name, data_dst_username, data_dst_ip, csv_dir)
	
#	while True:
#		try:
#			sp.call(cmd,shell=True)
#			break
#		except:
#			print "could not send "+file_name	
#			logging.warning("could not send file %s" % (file_name))

	while True:
		
		#sp.call(cmd,shell=True)
		p = subprocess.Popen([cmd], shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		stdout = p.stdout
		stderr = p.stderr
		if stderr.readline() != '':
			print "could not send "+file_name
			logging.warning("could not send file %s" % (file_name))

		else:
			print "sent "+file_name
			logging.warning(" sent file %s" % (file_name))
			break

		
	cmd = "rm "+file_name		#deleting local copy of csv
	subprocess.call(cmd,shell=True)



def main():
	logging.warning("starting")
	vmName 			= str(sys.argv[1])
	sleep_time 		= int(sys.argv[2])
	size_of_slice 		= int(sys.argv[3])
	apache_log_fetch_size 	= str(sys.argv[4])
	apache_log_sample_size 	= str(sys.argv[5])
	csv_dir			= str(sys.argv[6])
	data_dst_username	= str(sys.argv[7])
	data_dst_ip		= str(sys.argv[8])
	
	old_qos = collect_QOS()
	sliced_result = []	#for sliced data collect info. by jaydeep
	slice_no = 1		#counter for slice number. by jaydeep
	i = 1	#counter for observation no. count.by jaydeep
	while True:				#collect the data infinitely
		time.sleep(sleep_time)
		stat = []
		new_qos = collect_QOS()
		#stat.append(time.ctime(int(time.time())))
		stat.append(calendar.timegm(time.gmtime()))
		net_rtt = subprocess.check_output(['./apacheLog.sh', apache_log_fetch_size , apache_log_sample_size])
				
		for j in range(len(new_qos)):
			a=int(new_qos[j])-int(old_qos[j])
			stat.append(a)

		stat.append(float(net_rtt))
		sliced_result.append(stat)	#appending qos data for sliced data collect. by jaydeep
		logging.warning("one row created %d" % (i))	
		if i%int(size_of_slice)==0:	#here we are creating sliced data collect for faster qos. by jaydeep
			file_name = "qos_of_"+vmName+"_"+str(slice_no)+".csv"	#write to sliced csv file.
			csvfile=open(file_name,'a')
			writer=csv.writer(csvfile)

			for row in sliced_result:
				writer.writerow(row)

			csvfile.close()	
			print "file created. "+"vm: "+vmName+"obs: "+str(i)+"slice_no: "+str(slice_no)
			logging.warning("file created %d" % (slice_no))
			send_gen_file(file_name, csv_dir, data_dst_username, data_dst_ip)
				
			slice_no+=1		#increment the slice number counter
			sliced_result = []	#reinitializing the sliced result list to empty(0).by jaydeep
			
		old_qos=new_qos
		i+=1	#increment observation counnt.by jaydeep

if __name__=='__main__':
	main() 

