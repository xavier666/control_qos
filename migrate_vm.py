#this file is created since ovirt api was creating some problem when called. by jaydeep


from ovirtsdk.api import API
from ovirtsdk.xml import params
import subprocess
import random
import time
import sys
import paramiko
import threading
import csv
from multiprocessing import Process
import vm_config
import host_config
import vm_data_collect
import vm_qos_analysis
import random
import logging
import vm_bandwidth_modifier_2


logging.basicConfig(level=logging.WARNING,format='%(asctime)s %(created)d  %(message)s',filename='vm_monitor.log',filemode='a')

URL             = host_config.get_ovirt_interface_url()
USERNAME        = host_config.get_ovirt_interface_login()[0]
PASSWORD        = host_config.get_ovirt_interface_login()[1]


def wait(vm_name):	# wait till the VM has been migrated and up again
	api = API(url=URL, username=USERNAME, password=PASSWORD , insecure=True)
	#Obtaining vm service
	vm=api.vms.get(vm_name)
	state=vm.status.state
	api.disconnect

	while state != 'up':
		#Establishing Connection
		api2=API(url=URL, username=USERNAME, password=PASSWORD , insecure=True)
		#Obtaining vm service
		vm=api2.vms.get(vm_name)

		state=vm.status.state

		api2.disconnect()
		time.sleep(2)

	print "VM "+vm_name+' has migrated'
	logging.warning("VM "+vm_name+' has migrated')
	current_no_of_migration_data = vm_bandwidth_modifier_2.read_from_csv("current_no_of_migration.csv")
	for row in current_no_of_migration_data:
		if row[0] == vm_name:
			val = int(row[1])
			val+= 1
			row[1] = str(val)
	vm_bandwidth_modifier_2.write_to_csv (current_no_of_migration_data, "current_no_of_migration.csv")

# this method supports "migrateVM" method to find suitable hostd_engine and works only when no empty hosted_engine is available
def migrate_to_suitable_host(VM,load_type,total_no_of_host):
	api = API(url=URL, username=USERNAME, password=PASSWORD , insecure=True)	# handler from ovirt library
	vm_copy=api.vms.get(VM)

    # list to hold name of hosted engine and
	# no. of VMs of specified load type
	stat = []
	print "searching for suitable host. load type: "+load_type
	for i in range(1,total_no_of_host+1):
		no_of_vm = 0
		hostName = "hosted_engine_"+str(i)

		# finding out names of VMs currently present
		# in the target hosted_engine
		vm_on_host = vm_bandwidth_modifier_2.get_vm_on_host(hostName)

		# loop to find out no. of VMs of specified load type
		# present in the target hosted_engine
		for vm in vm_on_host:
			if str(vm[1]) == load_type:
				no_of_vm +=1

		# appends the hosted_engine name and no. of vm of
		# specified load type to stat list
		stat.append([hostName,no_of_vm])

	# sorting the content of stat list in ascending order of
	# no of vms of secified load type present in the hosted engine
	stat.sort(key = lambda x : x[1])

	# picking up the name of first hosted engine in the stat list
	# since it contents no or less VMs of specifed load type
	hostName = str(stat[0][0])
	print "found a suitable host: "+hostName

	try:
		# migrating the vm to the selected empty hosted_engine
		vm_copy.migrate(action=params.Action(host=params.Host(name=hostName,)),)

		print "migration started for %s ..." , VM
		logging.warning("migration started for %s ..." , VM)

		# === Sumitro
		# right now, we are at source server
		# so only dealloc the migrating vm b/w
		# 1. get bw of migrated vm
		# 2. get bw of vms in current server
		# 3. redistribute as per algorithm

		wait(VM)

		# === Sumitro
		# at this point, the VM has arrived at the destination server
		# apply the b/w realloc at the destination_host
		# 1. get old bw of migrated vm
		# 2. get bw of vms in destination server
		# 3. redistribute as per algorithm

		# challenges that need to be solved is gather_data
		# 1. get the global list of all server/vm over here
		# 2. make the functions in a different file so that recursive imports dont occur

	except Exception as e:
		print 'error in hosted_engine:\n%s' % str(e)
		logging.warning('error in hosted_engine:\n%s' % str(e))

#this method migrate vm to a specified host_engine
def migrateVM (VM,host) :
	print "inside migrateVM function "+VM+" "+host
	api = API(url=URL, username=USERNAME, password=PASSWORD , insecure=True)	# handler from ovirt library
	vm=api.vms.get(VM)
	vm_list = vm_bandwidth_modifier_2.read_from_csv("vm_list_info.csv")	#list contains all vm names and their load type
	total_no_of_host = 3		#this parameter holds total number of hosted engine present in the data centre

	for i in range(1,total_no_of_host+1):	# loop to search for totally empty hosted engine in the data centre
		hostName = "hosted_engine_"+str(i)
		vm_on_host = vm_bandwidth_modifier_2.get_vm_on_host(hostName) 	#finding out total no. of VMs currently present in the target hosted_engine

		if not vm_on_host:	# condition when a empty hosted engine has been iddentified
			print "found empty hosted_engine. name: "+hostName
			try:
				vm.migrate(action=params.Action(host=params.Host(name=hostName,)),)	# migrating the vm to the selected empty hosted_engine
				print "migration started for %s ..." , VM
				logging.warning("migration started for %s ..." , VM)
				#time.sleep(5)
				wait(VM)
				return

			except Exception as e:
				print 'error in hosted_engine:\n%s' % str(e)
				logging.warning('error in hosted_engine:\n%s' % str(e))

	print "not found any empty hosted_engine."

	#bellow lines execute only when no empty hosted_engine is present
	load_type = ""

	for vm in vm_list:	#finding out the load type of target vm
		if VM == vm[0]:
			load_type = str(vm[1])

	if load_type == "disk" or load_type == "network":
		#supporting method which helps to find out suitable hosted_engine when no empty hosted_engine is available
		migrate_to_suitable_host(VM,load_type,total_no_of_host)

def get_hostname(VM) :
	api = API(url=URL, username=USERNAME, password=PASSWORD , insecure=True)
	hostname = api.hosts.get(id=api.vms.get(VM).get_host().get_id()).get_name()
	api.disconnect()
	return hostname
