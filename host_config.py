host_user = {
		"hosted_engine_1" : "user",
		"hosted_engine_2" : "user1",
		"hosted_engine_3" : "user2" }

host_password = {
		"hosted_engine_1" : "user",
		"hosted_engine_2" : "user1",
		"hosted_engine_3" : "user2" }

host_ip = {
		"hosted_engine_1" : "10.5.20.121",
		"hosted_engine_2" : "10.5.20.122",
		"hosted_engine_3" : "10.5.20.123" }

host_gluster_port = {
		"hosted_engine_1" : ["49134", "49135"],
		"hosted_engine_2" : ["49130", "49141"],
		"hosted_engine_3" : ["49085", "49075"] }


hosts = [
		'hosted_engine_1', 
		'hosted_engine_2', 
		'hosted_engine_3']

full_bandwidth_all_networks 		= 950.0
gluster_synchronization_bandwidth 	= 0.0
full_bandwidth_with_disks 		= full_bandwidth_all_networks + gluster_synchronization_bandwidth

def get_ovirt_interface_login():
	login_details = ["admin@internal", "admin"]
	return login_details

def get_ovirt_interface_url():
	interface_url = "https://management.org/ovirt-engine/api"
	return interface_url

def get_data_dst_ip():
	management_svr_ip = "10.5.20.121"
	return management_svr_ip

def get_data_csv_dir():
	import os
	py_cwd = os.getcwd()
	return py_cwd + "/qos_archive"

def get_data_dst_username():
	management_svr_username = "user"
	return management_svr_username

def get_user(host) : 
	return host_user[host]

def get_password(host) : 
	return host_password[host]

def get_hostip(host) : 
	return host_ip[host]

def get_gluster_port(host):
	return host_gluster_port[host]
		
def get_hosts():
	return hosts
