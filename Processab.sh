#here $1 is used for filename.extension to be fetched from apache server
#here $2 is used for ip address of apache server 

for i in {1..30}
do

      nohup ab -c 4 -n 6000000 $2/$1 >/dev/null 2>&1 &
      PROC_ID=$!

      while kill -0 "$PROC_ID" >/dev/null 2>&1; do
            sleep 5s
      done

      echo "network load TERMINATED.restarting it"

done

exit 0

#echo "file fetching"

