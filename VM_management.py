from ovirtsdk.api import API
from ovirtsdk.xml import params

URL             = host_config.get_ovirt_interface_url()
USERNAME        = host_config.get_ovirt_interface_login()[0]
PASSWORD        = host_config.get_ovirt_interface_login()[1]

#DC_NAME =       'my_datacenter'
#CLUSTER_NAME =  'my_cluster'
#HOST_NAME =     'my_host'
#STORAGE_NAME =  'my_storage'
#EXPORT_NAME =   'my_export'
#VM_NAME =       'my_vm'

#api = API(url=URL, username=USERNAME, password=PASSWORD)


def start_vm(VM_NAME):
	api = API(url=URL, username=USERNAME, password=PASSWORD , insecure=True)
	try:
    		if api.vms.get(VM_NAME).status.state != 'up':
        		print 'Starting VM'
        		api.vms.get(VM_NAME).start()
        		print 'Waiting for VM to reach Up status'
        		while api.vms.get(VM_NAME).status.state != 'up':
            			sleep(1)
    		else:
        		print 'VM already up'
	except Exception as e:
    		print 'Failed to Start VM:\n%s' % str(e)
	api.disconnect()

def stop_vm(VM_NAME):
	api = API(url=URL, username=USERNAME, password=PASSWORD , insecure=True)
	try:
    		if api.vms.get(VM_NAME).status.state != 'down':
        		print 'Stop VM'
        		api.vms.get(VM_NAME).stop()
        		print 'Waiting for VM to reach Down status'
        		while api.vms.get(VM_NAME).status.state != 'down':
            			sleep(1)
    		else:
        		print 'VM already down'
	except Exception as e:
    		print 'Stop VM:\n%s' % str(e)
	api.disconnect()

def restart_vm(VM_NAME):
	api = API(url=URL, username=USERNAME, password=PASSWORD , insecure=True)
	try:
		if api.vms.get(VM_NAME).status.state == 'up':
			print 'VM is up ... restarting'
			api.vms.get(VM_NAME).stop()
			while api.vms.get(VM_NAME).status.state != 'down':
            			sleep(1)
			print 'VM is down'
		
		if api.vms.get(VM_NAME).status.state == 'down':
			api.vms.get(VM_NAME).start()
			while api.vms.get(VM_NAME).status.state != 'up':
            			sleep(1)
			print 'VM has been rebooted..'
			return 1
		
	except Exception as e:
    		print 'Restart VM:\n%s' % str(e)
	api.disconnect()	


